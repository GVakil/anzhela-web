/** Основной файл скриптов проекта */

"use strict";

class Api {
  static url = '/backend/';

  static get(method) {
    return fetch(this.url + method).then(r => r.json());
  }
  
  static post(method, data) {
    return fetch(this.url + method, {
      method: 'POST',
      body: JSON.stringify(data)
    });
  }
}

class DataController {
  static products = [];
}

class Product {
  constructor(data) {
    if (data.id) this.id = data.id;
    if (data.name) this.name = data.name;
    if (data.cost) this.cost = parseInt(data.cost);
    if (data.discount) this.discount = parseInt(data.discount);
    if (data.isNew) this.isNew = data.isNew;
    if (data.img) this.img = data.img;
    if (data.category) this.category = data.category;
    this.number = data.number ? data.number : 1;
  }

  static minusProduct($target) {
    const $product = $target.closest('.cartProduct');
    const product = CartController.getProduct($product.dataset.productId);
    product.number = product.number > 1 ? product.number - 1 : 1;
    product.updateElementForCart($product);
    CartController.updateTotal();
  }
  
  static plusProduct($target) {
    const $product = $target.closest('.cartProduct');
    const product = CartController.getProduct($product.dataset.productId);
    product.number++;
    product.updateElementForCart($product);
    CartController.updateTotal();
  }

  createElement() {
    const element = document.createElement('div');
    element.className = 'product-item ' + this.category;
    element.innerHTML = `
      <div class="product ${this.discount ? 'discount' : ''} product_filter">
        <div class="product_image">
          <img src="${this.img}" alt="">
        </div>
        <div class="favorite favorite_left"></div>
        <div class="product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center ${this.discount == 0 ? 'hidden' : ''}">
          <span>-${this.discount}%</span>
        </div>
        <div class="product_info">
          <h6 class="product_name">${this.name}</h6>
          <div class="product_price">${this.cost}Р<span>${this.cost * (this.discount + 100) / 100}</span></div>
        </div>
      </div>
      <div class="red_button add_to_cart_button"><a href="#" data-action="addToCart" data-product-id="${this.id}">В корзину</a></div>
    `;
    return element;
  }

  updateElementForCart($elem) {
    $elem.innerHTML = this.createElementForCart().innerHTML;
  }

  createElementForCart() {
    const element = document.createElement('div');
    element.className = 'cartProduct';
    element.dataset.productId = this.id;
    element.innerHTML = `
      <div class="cartProduct__img">
        <img src="${this.img}" alt="">
      </div>
      <div class="cartProduct__content">
        <div class="cartProduct__name">${this.name}</div>
        <div class="product_price">${this.number} x ${this.cost}Р</div>
        <div class="mt-auto d-flex justify-content-between" role="group">
          <div>
            <button type="button" class="btn btn-secondary btn-sm" style="font-size: 12px;" data-action="minusProduct"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-secondary btn-sm" style="font-size: 12px;" data-action="plusProduct"><i class="fa fa-plus"></i></button>
          </div>
          <div class="cartProduct__total">
            <div class="product_price" data-total="${this.cost * this.number}">${this.cost * this.number}Р</div>
          </div>
        </div>
      </div>
      <div class="cartProduct__controls">
        <div class="cartProduct__control" data-action="deleteProduct">
          <i class="fa fa-times"></i>
        </div>
      </div>
    `;
    return element;
  }

  print(parentNode) {
    parentNode.append(this.createElement());
  }
}

class AuthController {
  static USER_LOGGED_IN = true;
  static USER = {
    name: 'Инкогнито',
    phone: '89999999999'
  };

  static login(data) {
    Api.post('login.php', data).then((res) => {
      if (res.status !== 200) {
        alert('Неверный логин или пароль!');
        return;
      }
      
      this.USER_LOGGED_IN = true;
      this.USER = {
        name: data.name,
        phone: data.phone
      };
      (new Modal('#loginModal')).hide();
    });
  }

  static signup(data) {
    Api.post('signup.php', data).then(() => {
      (new Modal('#signupModal')).hide();
      (new Modal('#loginModal')).show();
    });
  }

  static logout() {
    this.USER_LOGGED_IN = false;
  }
}

class ChangesController {
  static showElems(elems) {
    elems.forEach(elem => {
      elem.classList.remove('hidden');
    });
  }
  
  static hideElems(elems) {
    elems.forEach(elem => {
      elem.classList.add('hidden');
    });
  }

  static checkCartProductsNumber() {
    const $elem = document.querySelector('[data-cart-products-number]');
    $elem.textContent = CartController.products.length;

    if (CartController.products.length === 0) {
      $elem.classList.add('hidden');
    } else {
      $elem.classList.remove('hidden');
    }
  }

  static check() {
    const loggedInElems = document.querySelectorAll('[data-user-logged-in]');
    const logoutElems = document.querySelectorAll('[data-user-logout]');

    if (AuthController.USER_LOGGED_IN) {
      this.showElems(loggedInElems);
      this.hideElems(logoutElems);
    } else {
      this.showElems(logoutElems);
      this.hideElems(loggedInElems);
    }

    this.checkCartProductsNumber();
  }

  static init() {
    this.check = this.check.bind(this);
    setInterval(this.check, 500);
  }
}

class CartController {
  static products = [];
  static $productList = document.querySelector('#cartModal .cartProducts');
  static $total = document.querySelector('#cartModal .cartTotal__right span');

  static getOrderLine() {
    return this.products.map(p => p.name + ' ' + p.number + ' шт').join('; ');
  }

  static getTotal() {
    return this.products.reduce((s, p) => s += p.cost * p.number, 0);
  }

  static updateTotal() {
    this.$total.textContent = this.getTotal();
  }

  static deleteProduct($elem) {
    const $product = $elem.closest('.cartProduct');
    const productId = +$product.dataset.productId;

    this.products = this.products.filter(p => p.id !== productId);
    Array.from(this.$productList.children).find($p => $p.dataset.productId == productId)?.remove();
  }

  static getProduct(productId) {
    return this.products.find(p => p.id == productId);
  }

  static addProduct($target) {
    const product = DataController.products.find(p => p.id == $target.dataset.productId);

    if (product) {
      this.products.push(product);
      this.$productList.append(product.createElementForCart());
      this.updateTotal();
    }
  }
}

class ActionController {
  static actions = [
    {
      type: 'logout',
      fn: AuthController.logout.bind(AuthController)
    },
    {
      type: 'addToCart',
      fn: CartController.addProduct.bind(CartController)
    },
    {
      type: 'minusProduct',
      fn: Product.minusProduct.bind(Product)
    },
    {
      type: 'plusProduct',
      fn: Product.plusProduct.bind(Product)
    },
    {
      type: 'deleteProduct',
      fn: CartController.deleteProduct.bind(CartController)
    },
  ];

  static init() {
    document.addEventListener('click', (e) => {
      const target = e.target.closest('[data-action]');

      if (!target) return;

      if (target instanceof HTMLAnchorElement) e.preventDefault();

      const action = this.actions.find(action => action.type === target.dataset.action);

      if (action) {
        action.fn(target);
      }
    });
  }
}

class Modal {
  constructor(selector) {
    this.$elem = document.querySelector(selector);
  }

  show() {
    $(this.$elem).modal('show');
  }
  
  hide() {
    $(this.$elem).modal('hide');
  }
}

class Forms {
  constructor() {
    this.loginForm = document.querySelector('#loginForm');
    this.signupForm = document.querySelector('#signupForm');
    this.newsForm = document.querySelector('#newsForm');
    
    this.loginReason = this.loginReason.bind(this);
    this.signupReason = this.signupReason.bind(this);
    this.newsReason = this.newsReason.bind(this);
    this.orderReason = this.orderReason.bind(this);

    this.initSubmitEvents();
  }

  loginReason(event) {
    const data = {
      phone: this.loginForm.phone.value,
      password: this.loginForm.password.value
    };

    AuthController.login(data);
  }
  
  signupReason(event) {
    const data = {
      name: this.signupForm.name.value,
      phone: this.signupForm.phone.value,
      password: this.signupForm.password.value
    };

    AuthController.signup(data);
  }
  
  newsReason(event) {
    alert('Вы успешно подписались на новости!');
  }
  
  orderReason(event) {
    const data = {
      name: AuthController.USER.name,
      phone: AuthController.USER.phone,
      cart: CartController.getOrderLine(),
      total: CartController.getTotal()
    };

    Api.post('order.php', data).then(() => {
      (new Modal('#cartModal')).hide();
      (new Modal('#thanksModal')).show();
    });
  }

  initSubmitEvents() {
    document.addEventListener('click', (event) => {
      if (event.target.closest('[data-submit=loginForm]')) this.loginReason();
      else if (event.target.closest('[data-submit=signupForm]')) this.signupReason();
      else if (event.target.closest('[data-submit=newsForm]')) this.newsReason();
      else if (event.target.closest('[data-submit=orderForm]')) this.orderReason();
    });
  }
}

async function printProducts() {
  const productsList = document.querySelector('#productsList');

  const productsData = await Api.get('products.php');

  const products = productsData.map(data => new Product(data));

  DataController.products = products;

  products.forEach(product => {
    product.print(productsList);
  });
}

async function loadScript(src) {
  const script = document.createElement('script');
  script.src = src;
  document.body.append(script);
  
  return new Promise((res, rej) => {
    script.addEventListener('load', () => {
      res();
    });
  });
}

async function start() {
  await printProducts();
  await loadScript('/js/lib/jquery-3.2.1.min.js');
  await loadScript('/styles/lib/bootstrap4/popper.js');
  await loadScript('/styles/lib/bootstrap4/bootstrap.min.js');
  await loadScript('/plugins/Isotope/isotope.pkgd.min.js');
  await loadScript('/plugins/OwlCarousel2-2.2.1/owl.carousel.js');
  await loadScript('/plugins/easing/easing.js');
  await loadScript('/js/lib/custom.js');

  const forms = new Forms();

  ChangesController.init();
  ActionController.init();
}

start();
