-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 13 2021 г., 10:34
-- Версия сервера: 5.6.51
-- Версия PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `coffeeshop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `order_products`
--

CREATE TABLE `order_products` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Без названия',
  `cost` int(11) NOT NULL DEFAULT '0',
  `discount` int(11) NOT NULL DEFAULT '0',
  `is_new` int(1) NOT NULL DEFAULT '0',
  `img` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'COFFEE'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `name`, `cost`, `discount`, `is_new`, `img`, `category`) VALUES
(1, 'Раф', 100, 0, 0, '/images/products/raf.jpg', 'coffee'),
(2, 'Капучино', 100, 0, 0, '/images/products/kapuchino.jpg', 'coffee'),
(3, 'Латте', 100, 0, 0, '/images/products/latte.jpg', 'coffee'),
(4, 'Флэт Уайт', 100, 0, 0, '/images/products/flat.jpg', 'coffee'),
(5, 'Американо', 100, 0, 0, '/images/products/americano.jpg', 'coffee'),
(6, 'Эспрессо', 100, 0, 0, '/images/products/espresso.jpg', 'coffee'),
(7, 'Горячий шоколад', 100, 0, 0, '/images/products/chokolate.jpg', 'other'),
(8, 'Какао', 100, 0, 0, '/images/products/kakao.jpg', 'other'),
(9, 'Чай', 100, 0, 0, '/images/products/tee.jpg', 'other'),
(10, 'Раф Птичье молоко', 100, 0, 0, '/images/products/exclusive_1.jpg', 'exclusive'),
(11, 'Раф Малина-кокос', 100, 0, 0, '/images/products/exclusive_2.jpg', 'exclusive'),
(12, 'Капучино Бельгийская вафля', 100, 0, 0, '/images/products/exclusive_3.jpg', 'exclusive'),
(13, 'Латте Груша с соленой карамелью', 100, 0, 0, '/images/products/exclusive_4.jpg', 'exclusive');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `password` varchar(2048) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `password`, `name`, `phone`) VALUES
(1, '$2y$10$ejEExTDZJnObLeR5aKw5S.o.NGeEEipk1z9jMQMcUArXrFwMeCH6q', '121', '12121'),
(2, '$2y$10$tKPx4r81xSSnIwO6qY2b0ObxqEx2ucE5Vt4sG06Aal/RXW5coD7bi', '12121', '121211'),
(3, '$2y$10$stbevIpAxKoS62l1U3jDJOY.ApC/nqPONgBeG/oZOq9nuAAkLo9sO', '121', '12121212'),
(4, '$2y$10$iRO.9n9zxoH8loUafJ9HSO/FyCtr9G05.x9V9qZmVx43u/O0.g1na', 'Вакиль', '89999999999'),
(5, '$2y$10$bQBF2hN7skCn3PQmyxCJTes2sh3zu4wfBBKydaqXrTwTwuqGD8Kia', 'test', '89999999998');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
