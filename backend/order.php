<?php

require_once __DIR__ . '/config.php';

$requestBody = json_decode(file_get_contents("php://input"));

try {
  $admin = "anzhela_moon@mail.ru";  // e-mail админа 

  $phone = trim($requestBody->phone);
  $cart = trim($requestBody->cart);
  $total = trim($requestBody->total);

  // данные с полей
  $data = "";

  if (isset($phone)) {
    $data .= "Телефон: $phone<br>";
  }
  if (isset($cart)) {
    $data .= "Корзина: $cart<br>";
  }
  if (isset($total)) {
    $data .= "Итого: {$total}Р<br>";
  }


  $subject = 'Поступила заявка с сайта Coffee Shop';
  $message = '
      <html>
          <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
              <title>Тема страницы</title>
          </head>
          <body>
              <h4 style="color: #333">Заявка с сайта <a style="color: #333" href="https://coffeeshop.ru/">Coffee Shop</a></h4>' .
    $data
    . '</body>
      </html>';

  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= "Content-type: text/html; charset=utf-8 \r\n";

  // дополнительные данные
  $headers .= "From: CoffeeShop <admin@coffeeshop.ru>\r\n";
  $result = mail($admin, $subject, $message, $headers);
  
  response($result);
} catch (\Throwable $th) {
  response($th->getMessage(), 400);
}
