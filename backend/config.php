<?php

require_once __DIR__ . '/libs/rb-mysql.php';

// разрешение "_" в названиях таблиц
R::ext('xdispense', function($type) { 
  return R::getRedBean()->dispense($type); 
});

$db_host = "localhost";
$db_name = "coffeeshop";
$db_user = "root";
$db_password = "";

R::setup("mysql:host=$db_host;dbname=$db_name", $db_user, $db_password, true);

function response($data, $code = 200) {
  http_response_code($code);
  exit(json_encode($data));
}
