<?php

require_once __DIR__ . '/config.php';

$requestBody = json_decode(file_get_contents("php://input"));

$user = R::findOne('users', 'phone = ?', [$requestBody->phone]);

if (!$user) {
  response('Пользователь не найден', 400);
}

if (!password_verify($requestBody->password, $user->password)) {
  response('Пароль неверный', 400);
}

response($user);
