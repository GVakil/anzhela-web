<?php

require_once __DIR__ . '/config.php';

try {
  $products = R::getAll( 'SELECT * FROM products' );
  response($products);
} catch (\Throwable $th) {
  response($th->getMessage(), 400);
}
