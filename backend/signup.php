<?php

require_once __DIR__ . '/config.php';

$requestBody = json_decode(file_get_contents("php://input"));

try {
  $user = R::xdispense('users');
  $user->name = $requestBody->name;
  $user->phone = $requestBody->phone;
  $user->password = password_hash($requestBody->password, PASSWORD_DEFAULT);
  R::store($user);
  response($user->id);
} catch (\Throwable $th) {
  response($th->getMessage(), 400);
}
